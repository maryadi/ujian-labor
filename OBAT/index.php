<!DOCTYPE html>
<html>
<head>
    <title>Pendaftaran Siswa Baru | SMK Coding</title>
    <style>
        .center {
        margin: auto;
        width: 50%;
        background-color: #00a3cc;
        padding: 10px;
        }
        .button { 
            background-color: #00CCFF;
            padding: 8px 16px;
            display: inline-block;
            text-decoration: none;
            color: #FFFFFF border-radius: 3px;
        }

        .button:hover { 
            background-color: #0066FF; 
        }
    </style>
</head>

<body>

    <div class="center">
        <center>
            <h1>TOKO APOTIK SURYA</h1>
            <a href="halaman_obat.php" class="button">Obat</a>        
            <a href="lihat_transaksi.php" class="button">Transaksi</a>
        </center>
        
    </div>   
    
    </body>
</html>