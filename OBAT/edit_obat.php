

<?php

include "koneksi.php";

// kalau tidak ada id di query string
if( !isset($_GET['id']) ){
    header('Location: halaman_obat.php');
}

//ambil id dari query string
$id = $_GET['id'];

// buat query untuk ambil data dari database
$sql = "SELECT * FROM obat WHERE kdobat='$id'";
$query = mysql_query($sql);
$obat = mysql_fetch_assoc($query);

// jika data yang di-edit tidak ditemukan
if( mysql_num_rows($query) < 1 ){
    die("data tidak ditemukan...");
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>ding</title>
</head>

<body>
    <center>
        <h1>Edit Data Obat</h1>
    </center>

    <form action="proses_edit.php" method="POST">

        <fieldset>

        <p>
            <label for="nama">Kode Obat: </label>
            <input type="text" name="kdobat" value="<?php echo $obat['kdobat']; ?>" />
        </p>
        <p>
            <label>Nama Obat: </label>
            <input type="text" name="nmobat" value="<?php echo $obat['nmobat']; ?>">
        </p>
        <p>
            <label>Jenis Obat: </label>
            <?php $obatnya = $obat['jnsobat']; ?>
            <select name="jnsobat">
                <option <?php echo ($obatnya == 'OBAT RINGAN') ? "selected": "" ?>>OBAT RINGAN</option>
                <option <?php echo ($obatnya == 'OBAT SEDANG') ? "selected": "" ?>>OBAT SEDANG</option>
                <option <?php echo ($obatnya == 'OBAT KERAS') ? "selected": "" ?>>OBAT KERAS</option>
            </select>
        </p>
        <p>
            <label>Harga Obat: </label>
            <input type="text" name="hrgobat" value="<?php echo $obat['hrgobat']; ?>">
        </p>
        <p>
            <label>Stok Obat: </label>
            <input type="text" name="stok" value="<?php echo $obat['stok']; ?>"/>
        </p>
        <p>
            <input type="submit" value="Simpan Data Obat" name="edit" />
        </p>

        </fieldset>

    </form>

    </body>
</html>